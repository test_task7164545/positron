import { createRouter, createWebHistory} from 'vue-router'
import MainPage from '@/pages/MainPage.vue'
// import CartPage from '@/pages/MainPage.vue'

const routes = [
  {
    path: '/',
    name: 'main',
    component: MainPage
  },
  {
    path: '/cart',
    name: 'cart',
    component: () => import(/* webpackChunkName: "about" */ '@/pages/CartPage.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
