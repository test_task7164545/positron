import { createStore } from "vuex";
// import axios from "axios"
export default createStore({
  state: {
    // currencyCourse: null,
    // currencyList: "EURRUB",
    // rates: "rates",
    // Положу его сюда,хотя лучше бы в конфиг
    // API_KEY_CURRRENCY: 'a4b49a67088db459cd6f5f0925cd8ec0',

    sendOrderSuccess: false,
    sendOrderFailed: false,
    isInstall: false,
    priceInstall: 1200,
    productsCart: [
      {
        id: 1,
        title: "Вытяжное устройство G2H",
        description: {
          descr1: "12-72/168 м3/ч",
          descr2: "гидрорегулируемый расход",
          descr3: "от датчика присутствия",
        },
        gallery: { url: "/img/cart-products/G2H.png" },
        price: 12644,
        quantity: 10,
      },
      {
        id: 2,
        title: "Вытяжное устройство BXC",
        description: {
          descr1: "12-72/167 м3/ч",
          descr2: "гидрорегулируемый расход",
          descr3: "от датчика присутствия",
        },
        gallery: {
          url: "/img/cart-products/BXC.png",
        },
        price: 15324,
        quantity: 8,
      },
      {
        id: 3,
        title: "Вытяжное устройство GHN",
        description: {
          descr1: "13-75/197 м3/ч",
          descr2: "гидрорегулируемый расход",
          descr3: "от датчика присутствия",
        },
        gallery: {
          url: "/img/cart-products/GHN.png",
        },
        price: 21744,
        quantity: 8,
      },
    ],
    productsOrder: [],
    swiperData: [
      {
        id: 1,
        title: "G2H",
        description: "Многофункциональное вытяжное устройство для естественной и гибридной вентиляции",
        gallery: { url: "/img/cart-swiper/1.png" },
        priceFrom: 6848,
        priceTo: 56584,
      },
      {
        id: 2,
        title: "BXC",
        description: "Вытяжное устройство для механической системы вентиляции",
        gallery: { url: "/img/cart-swiper/2.png" },
        priceFrom: 6899,
        priceTo: 52586,
      },
      {
        id: 3,
        title: "GHN",
        description: "Вытяжное устройство с датчиком присутствия",
        gallery: { url: "/img/cart-swiper/3.png" },
        priceFrom: 4899,
        priceTo: 42586,
      },
      {
        id: 4,
        title: "TDA",
        description: "Вытяжное устройство с датчиком присутствия",
        gallery: { url: "/img/cart-swiper/4.png" },
        priceFrom: 899,
        priceTo: 2586,
      },
      {
        id: 5,
        title: "G2H",
        description: "Многофункциональное вытяжное устройство для естественной и гибридной вентиляции",
        gallery: { url: "/img/cart-swiper/1.png" },
        priceFrom: 6848,
        priceTo: 56584,
      },
      {
        id: 6,
        title: "BXC",
        description: "Вытяжное устройство для механической системы вентиляции",
        gallery: { url: "/img/cart-swiper/2.png" },
        priceFrom: 16899,
        priceTo: 22586,
      },
      {
        id: 7,
        title: "GHN",
        description: "Вытяжное устройство с датчиком присутствия",
        gallery: { url: "/img/cart-swiper/3.png" },
        priceFrom: 14899,
        priceTo: 22586,
      },
      {
        id: 8,
        title: "TDA",
        description: "Вытяжное устройство с датчиком присутствия",
        gallery: { url: "/img/cart-swiper/4.png" },
        priceFrom: 4899,
        priceTo: 42586,
      },
    ]
  },
  getters: {
    productsSwiper(state) {
      return state.swiperData
    },
    products(state) {
      return state.productsCart
    },
    productsCount(state, getters) {
      return getters.products.reduce((acc, item) => acc + item.quantity, 0)
    },
    priceInstall(state, getters) {
      if (state.isInstall) {
        return (state.priceInstall * getters.productsCount)
      } else return 0
    },
    priceProducts(state, getters) {
      return getters.products.reduce((acc, item) => acc + item.price * item.quantity, 0)
    },
    totalPrice(state, getters) {
      return (getters.priceInstall + getters.priceProducts)
    }
  },
  mutations: {
    // changeCourse(state, result) {
    //   state.currencyCourse = result
    // },
    changeOrderSuccess(state, boolean) {
      state.sendOrderSuccess = boolean
    },
    changeOrderFailed(state, boolean) {
      state.sendOrderFailed = boolean
    },
    updateInstall(state, boolean) {
      state.isInstall = boolean
    },
    updateCartProductAmount(state, { basketId, quantity }) {
      const product = state.productsCart.find((item) => item.id === basketId);
      if (product) {
        product.quantity = quantity
      } else {
        state.productsCart.push({ basketId, quantity })
      }
    },
    deleteProductFromCart(state, id) {
      state.productsCart = state.productsCart.filter(item => item.id !== id)
    },
    resetCart(state) {
      state.productsCart = [];
    },
    updateProductsOrder(state, product) {
      state.productsOrder.push(product)
    },
  },
  actions: {
    // getCurrentCourse(context) {
    //   axios.get("https://currate.ru/api/", {
    //     params: {
    //       get:context.state.rates,
    //       key: context.state.API_KEY_CURRRENCY,
    //       pairs: context.state.currencyList,
    //     },
    //   })
    //     .then(() => console.log('ok'))
    //     .catch(()=>'error')
    // },
    sendOrder(context) {
      return (new Promise(resolve => {
        resolve()
        setTimeout(() => {
          this.getters.products.map(item => {
            context.commit('updateProductsOrder', {
              ...item,
              comment: 'www',
              install: this.state.isInstall,
            })
            context.commit('resetCart')
            context.commit('changeOrderSuccess', true)

          })
        }, 2000)
      })
        .catch(() => {
          context.commit('changeOrderSuccess', false)
          context.commit('changeOrderFailed', true)
        })
      )
    }
  },
});

