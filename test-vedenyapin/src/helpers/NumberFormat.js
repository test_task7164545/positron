// ! параметром передаем число, которое вернется отформатированным со знаком рубля
export default function NumberFormat(price, currency='RUB') {
  return new Intl.NumberFormat("ru-Ru", { notation: "standard", style: 'currency', currency, maximumFractionDigits: 0 }).format(price);
}