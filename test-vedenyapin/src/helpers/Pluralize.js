
// !в функции первым аргументом идет массив со словами. 0 элемент массива- слово в единственном числе, 1 элемент - товары в родительном падеже(множественное число для пары), 3 элемент - множественное число, вторым аргументом - число, от которого зависит множестенность из массива

function Plural(arrWords, count) {
  let ending = String(count).split('')
  let secEnd = ending.slice(-2, -1)

  function defineEnding() {
    if (ending.length >= 2) {
      if (Number(secEnd[0]) !== 1) {
        return end()
      } else {
        return arrWords[2]
      }
    } else {
      return end()
    }
  }
  return defineEnding()

  function end() {
    if (ending.slice(-1)[0] === '1') {
      return arrWords[0]
    } else if (ending.slice(-1)[0] === '0' || Number(ending.slice(-1)[0]) <= 9 && Number(ending.slice(-1)[0]) >= 5) {
      return arrWords[2]
    } else {
      return arrWords[1]
    }
  }
}



export default Plural